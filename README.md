# Realtime Render Suite

## What does it do?
The Realtime Render Suite is a collection of tools to easily render out animations from unity with options for object ID matte passes.

## How do I install it?
Go to Assets > Import Package and import the RenderSuite.unitypackage file from this repository. (Already installed in AutoSergei project!)

## How do I play back an animation?
To play back an animation, make sure you have a RenderSuite prefab in the scene.

1. Set **Mode** to *Playback*
2. Set **Target** to the root character object in the scene.
3. Set **Camera** to the main camera that is rendering the character.
4. Set **Clip** to an animation clip. Usually from the character's prefab.
5. If you are rendering the scene with multiple backgrounds, add them to the **Background Passes** array.
6. If you are rendering an Object ID pass (or a matte pass), check the **Include Object ID** box and add the Object ID Controller component to the RenderSuite object and drag it into the **Object ID Controller** slot on the the Motion Render component.

For each background pass and object ID pass, the Render Suite will play the animation from the start.

## How do I set up Object ID?

Object ID requires an Object ID Controller *(see step 6 in **How do I play back an animation**)* and an Object ID Color component added to each mesh object that you want rendered on the object ID pass.

Here's how to apply Object ID to a character's eyes:

1. Set up the Object ID Controller (see above)
2. Select the character's nose object in the scene hierarchy. (For example: nose_GEO)
3. Add the Object ID Color component
4. Select a colour from the list.

```Note: Object ID is currently cascading. This means that if you apply the Object ID Color component to an object in the characters's hierarchy, all subsequent objects will share that colour. To stop this happening, apply an Object ID Color to the next object in the hierarchy with the color: None. This will be fixed.```

If you want to apply a colour to the entire character, simply add the Object ID Color component to the root character object!

## How do I render?
To render out to video or PNG sequence, simply use Unity's Recorder Tool found in Window > Recorder. Passes will have to be separated manually after rendering.